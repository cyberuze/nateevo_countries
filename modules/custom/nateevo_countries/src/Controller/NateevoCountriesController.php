<?php

/**
 * @file
 * Contains \Drupal\nateevo_countries\Controller\NateevoCountriesController.
 */

namespace Drupal\nateevo_countries\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity;
use Drupal\nateevo_countries\Entity\Country;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;

/**
 * Main class provides a list controller used by nateevo_countries module.
 *
 * @ingroup nateevo_countries
 *
 */
class NateevoCountriesController extends ControllerBase {

	/**
	 * @param const URL_API
	 *   URL of the API Rest
	*/
	protected const URL_API = 'https://restcountries.com/v2/all';

	/**
	 * @param const VOCABULARY
	 *   Main parameters of the vocabulary for regions of the countries
	*/
	protected const VOCABULARY = [
		'vid' => 'continents',
		'machine_name' => 'continents',
        'description' => 'Continents Vocabulary',
        'name' => 'Continents'	
	];

	/**
	 * @param const DIR_FLAGS_IMAGES 
	 *   Directory to store flags images of the countries
	*/
	protected const DIR_FLAGS_IMAGES = 'sites/default/files/countries_flags/';

	/**
	 * Constructs a new vocabulary of taxonomy
     *
     * @param void
	*/
	protected function createVocabularyContinent() {
	
		// Check if the vocabulary exists
		$vocabularies = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();
		if(!isset($vocabularies[self::VOCABULARY['vid']])){
			$vocabulary = new Vocabulary(
				self::VOCABULARY, 
				'taxonomy_vocabulary');
			if($vocabulary->save()){
				\Drupal::messenger()->addMessage($this->t("Vocabulary %vocabulary has been created."), [
					'%vocabulary' => self::VOCABULARY['name']
				]);
			}			
		}
		else {			
			\Drupal::logger('nateevo_countries')->notice($this->t('Vocabulary %vocabulary already exists.'), [
					'%vocabulary' => self::VOCABULARY['name']
				]);
		}
	}

	/**
	 * Get countries list JSON from API Rest
     *
     * @param $url string 
	 * The url of API. Default is the property URL_API
	 *
	*/
	protected function getCountriesDataApi(string $url = self::URL_API) {		
		$curl_handle=curl_init();
		curl_setopt($curl_handle,CURLOPT_URL, $url);
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);	
		return json_decode($buffer);
	}

	/**
	 * Save a continent or region if not exists on the taxonomy Continents,
	 * in any case, even exists, return the term id
     *
     * @param $continent string 
	 * The name os the region of the country
	 * 
	*/
	protected function saveContinentTerm(string $continent){
		$term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
			'name' => $continent, 
			'vid' => self::VOCABULARY['vid'],
		]); 

		// if not exists term (continent) we will creating and return new tid
		if(empty($term)){
			$term = Term::create([
					'name' => $continent,
					'vid'  => self::VOCABULARY['vid'],
				])->save();
	
			return ($term);
		} 
		// if the term exists return the term id
		return key($term);		
	}

	/**
	 * Store a country as a Country entity and save it in database, but
	 * if the country is already stored, we delete it before save it again.
     *
     * @param $resource object 
	 * The json data of the country 
	 * 
	*/
	protected function saveCountry(object $resource) {
		
		// checking if country already exists
		$entity = \Drupal::entityTypeManager()
			->getStorage('country')
			->loadByProperties([
				'title' => $resource->name,
				]);
		
		if(empty($entity)){

			$country = [];

			// retrieve country's name
			$country['title'] = $resource->name;

			// retrieve country's region if exists and we creating its continent term
			if(isset($resource->region)){
				$country['field_continent'] = $this->saveContinentTerm($resource->region);
			}else{
				$country['field_continent'] = 0;
			}

			// retrieve country's capital if exists
			if(isset($resource->capital)){
				$country['field_capital'] = $resource->capital;
			}else{
				$country['field_capital'] = "";
			}

			// retrieve country's population if exists
			if(isset($resource->population)){
				$country['field_population'] = $resource->population;
			}else{
				$country['field_population'] = 0;
			}

			// retrieve country's area if exists
			if(isset($resource->area)){
				$country['field_area'] = $resource->area;
			}else{
				$country['field_area'] = 0;
			}

			// retrieve country's flag if exists and download it
			if(isset($resource->flag)){
				$country['field_flag'] = $this->saveCountryFlag($resource->flag);
			}else{
				$country['field_flag'] = "";
			}

			// The status of country, by default unpusblished
			$country['status'] = 0;

			// save country as a Country entity
			$entity = Country::create($country);
					
			$entity->save();
			
		} else {
			$entity = reset($entity);
			$entity->delete();
		}
		return $entity;
	}

	/**
	 * Download flag image svg to the directory given by const DIR_FLAGS_IMAGES,
	 * after, its url is stored in the entity country
     *
     * @param $image string 
	 * The flag image´s url of the country
	 * 
	*/
	protected function saveCountryFlag(string $image) {
		
		$directory = self::DIR_FLAGS_IMAGES;

		// if the directory not exists, we create it
		if (!is_dir($directory)) {
			mkdir ($directory, 0755, true);
		}
		// get the name of file image
		$flag = basename($image);

		// download flag image by curl and save to the flag´s directory
		$ch = curl_init($image);
		$fp = fopen($directory . $flag, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);

		return $flag;
	}

	/**
	 * Save each country received by API in the Country entity
     *
     * @param $resources array
     * Array with the countries data from the API
	*/
	protected function saveCountriesList(array $resources) {
		$entities = [];

		foreach ($resources as $resource) {										
			// for each country, we creating or retrieve its term id 
			$entity = $this->saveCountry($resource);
			$entities[] = $entity;			
		}	

		return $entities;
	}

	/**
   	  * Print a table with the countries data from Api rest
      *
	  * @param \Symfony\Component\HttpFoundation\Request $request;
	  *	 A request object
	  *
	  * @return array
	  *  An array by drupal_render().
	  *
     */	
	public function printCountriesList(Request $request) {

		$vid = $this->createVocabularyContinent();
		
		$resources = $this->getCountriesDataApi();
			
		if(!empty($resources)){
	
			$entities = $this->saveCountriesList($resources);
			
			// path base of flags countries folder
			$host = \Drupal::request()->getSchemeAndHttpHost() . '/' . self::DIR_FLAGS_IMAGES;

			$output = "<table>";
			$output .= "<tr style='font-weight:bold;'>";
			$output .=	"<td>Region</td><td>Flag</td><td>Name</td><td>Capital</td><td>Population</td><td>Density Pop.</td>";
			$output .= "</tr>";
			
			foreach ($entities as $entity) {
				
				$flag =  $host . $entity->field_flag->value;

				$density = 0;			
				if($entity->field_population->value != 0 && $entity->field_area->value !=0) {
					$density = $entity->field_population->value / $entity->field_area->value; 
				}

				$output .= "<tr style='line-height=50px;'>";
					$output .= "<td>" . $entity->field_continent->value . "</td>";					
					$output .= "<td><img src='" . $flag . "' style='width: 50px; height:auto;margin: 0 18px' /></td>";
					$output .= "<td>" . $entity->title->value . "</td>";					
					$output .= "<td>" . $entity->field_capital->value . "</td>";				
					$output .= "<td>" . number_format(floatval($entity->field_population->value), 0, ",", ".") . "</td>";
					$output .= "<td>". round(intval($density)) . "</td>";
				$output .= "</tr>";			
			}
			$output .= "</table>";
		}
		else {
			$output = "Error getting countries from API";
		}
	
		$html = Markup::create($output);

		return [
			'#theme' => 'nateevo_countries',
			'#page_title' => 'Countries List',
			'#content' => $html,
		];
	}

}