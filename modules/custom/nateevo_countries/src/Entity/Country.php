<?php

/**
 * @file
 * Contains \Drupal\nateevo_countries\Entity\Country.
 */

/**
 * Defines the Country entity.
 *
 * This file implements an entity called Country useful
 * to save each country getting from Country List API
 *
*/

namespace Drupal\nateevo_countries\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\nateevo_countries\CountryInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the country entity.
 *
 * @ingroup nateevo_countries
 *
 * @ContentEntityType(
 *   id = "country",
 *   label = @Translation("Country"),
 *   base_table = "nateevo_countries",
 *   entity_keys = {
 *     "id" = "id",
 *	   "uuid" = "uuid"
 *   }, 
 * )
 */
class Country extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Determines the schema for the base_table property defined above.
  */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('ID'))
        ->setDescription(t('The ID of the Country entity.'))
        ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Contact entity.'))
      ->setReadOnly(TRUE);

    // Title field for the country.
    $fields['title'] = BaseFieldDefinition::create('string')
        ->setLabel(t("The country's name"))
        ->setDescription(t('The name of the country.'))
        ->setSettings(array(
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ));

    // Continent field for the country.
    $fields['field_continent'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Continent'))
      ->setDescription(t('The continent of the country.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ));

    // Capital field for the country.
    $fields['field_capital'] = BaseFieldDefinition::create('string')
        ->setLabel(t("The country's capital"))
        ->setDescription(t('The capital of the country.'))
        ->setSettings(array(
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ));

    // Population field for the country.
    $fields['field_population'] = BaseFieldDefinition::create('string')
        ->setLabel(t("The country's population"))
        ->setDescription(t('The population of the country.'))
        ->setSettings(array(
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ));

    // area field for the country.
    $fields['field_area'] = BaseFieldDefinition::create('string')
        ->setLabel(t("The country's area"))
        ->setDescription(t('The area of the country.'))
        ->setSettings(array(
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ));

    // Flag image field for the country.
    $fields['field_flag'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Flag'))
      ->setDescription(t('An image representing the flag of the country.'))
      ->addPropertyConstraints('value', ['Image' => []]);

    // area field for the country.
  	$fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('Whether the country is published.'))
      ->setDefaultValue(FALSE);

    return $fields;
  }
}
