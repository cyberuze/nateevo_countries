<?php

/**
 * @file
 * Contains Drupal\nateevo_countries\Entity\Country.
 */

namespace Drupal\nateevo_countries;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Country entity.
 *
 * @ingroup nateevo_countries
 *
 * This interface can join the other interfaces it extends.
 *
 */
interface CountryInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {}